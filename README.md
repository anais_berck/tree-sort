# Tree sort

![A visualization of the binary search tree, the tree is constructed with the definition of the tree sort as found on Wikipedia](tree-sort.svg)

Repository for the workshop on the  [tree sort algorithm](https://en.wikipedia.org/wiki/Tree_sort).

You'll find the workshop material in the `workshop` folder.

## Recipes

Recipes used during the workshop to execute the tree sort algorithm manually (with a group of people).

### To insert a node within a tree

*This recipe inserts a new node into an (empty) tree. It is assumed there is a place defined where the tree will be started (rooting point?). When the recipe is executed the **current position** is considered to be rooting point.*

1. Look at your *current position*, if it's empty (there is no node) this will be your place: you are inserted in the tree, and the recipe is finished. If there is a node continue with step two.

2. Ask the node in the *current position* for its value.

3. Compare the answer of the node to your own value. Is your value smaller than theirs? Then follow the left arm of the node, and take that as the new *current position* and move back to step one. Otherwise continue with step four.

4. Is your own value the same as or bigger than the answer the node gave? Then follow the right arm of the node, and take that as your new *current position* and go back to step one.


### To measure the depth of a tree

*This recipe discovers the depth of the deepest branch in the (sub)tree. It is initiated by asking the root or any other node to measure its depth.*

1. Ask the node attached to your left arm for its depth, or take zero if you have none.

2. Ask the node attached to your right arm for its depth, or take zero if you have none.

3. Pick the greater one out of the answers you collected in step one and two, and add 1 (increment by one).

4. Answer the number you calculated in step 3 to the node which asked you to measure your depth.


### To traverse the tree in order:

*This recipe transforms the (sub)tree into an orderd row (or list)*. It is initiated by asking the root or any other node to traverse.

1. If you have a left node: ask it to traverse. Once it has finished traversing ask the resulting row (list) to stand to  the *left* of you. It is important the row maintains it order.

2. If you have a right node: ask it to traverse. Once it has finished traversing ask the resulting row (list) to stand to  the *right* of you. It is important the row maintains it order.

3. By combining the row on the left, yourself, and the row to your right a new row has formed. Tell the node who asked you to traverse that you're ready.


## Code implementations

In the folder `workshop/code` a simple implementation of the tree sort algorithm can be found.

### workshop/code/treesort.py

Implementation of the binary search tree. When the script is 

```
python workshop/code/treesort.py
```

### workshop/code/tree-visualizer.py

Visualizes a tree using graphviz

```
python workshop/code/tree_visualizer.py
```

### workshop/code/text-tree.py

Reads a text (`workshop/text-to-sort.txt`) and generates a visual tree:

```
python workshop/code/text_tree.py
```

## Requirements

The implementations are written in python and require python 3, find information on how to [install python here](https://www.python.org/downloads/)

The visualizer and the text_tree both use graphviz to generate the visualization. Find information on how to [install graphviz here](https://graphviz.org/download/)

The visualizer and the text_tree also require graphviz the [graphviz python bindings](https://pypi.org/project/graphviz). The easiest way to install them is through pip:

```
pip3 install graphviz
```

